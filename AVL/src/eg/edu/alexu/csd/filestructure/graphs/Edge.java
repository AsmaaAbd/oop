package eg.edu.alexu.csd.filestructure.graphs;

public class Edge implements Comparable<Edge>{
	
	public int to ;
	public Integer w ;
	
	public Edge(int to , Integer w){
		this.to = to ;
		this.w = w ;
	}

	@Override
	public int compareTo(Edge o) {
		return this.w - o.w;
	}
	

}
