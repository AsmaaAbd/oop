package eg.edu.alexu.csd.filestructure.avl;

public class Avl<T extends Comparable<T>> implements IAVLTree<T> {

	private Node<T> root;
	private final int allowedBallanceLimit = 1;
	private boolean ElementDeleted ; 

	public Avl() {
		root = null;
	}

	private int height(Node<T> t) {
		if (t == null)
			return 0;
		else {
			return t.height;
		}
	}
	
	@Override
	public void insert(T key) {
		insert(key, root);
	}

	private Node<T> insert(T key, Node<T> current) {
		if (current == null) {
			Node<T> temp = new Node<T>(key, null, null);
			temp.height = 1 ;
			if(root == null){
				root = temp ;
			}
			return temp ;
		}
		int comp = key.compareTo(current.getValue());
		if (comp < 0) {
			current.setLeftChild(insert(key, (Node<T>) current.getLeftChild()));
		} else {
			current.setRightChild(insert(key, (Node<T>) current.getRightChild()));
		}
		return balance(current);
	}

	private Node<T> balance(Node<T> current) {
		
		if (current == null)
			return current;
		if (height((Node<T>) current.getLeftChild())
				- height((Node<T>) current.getRightChild()) > allowedBallanceLimit) {
			if (height((Node<T>) current.getLeftChild().getLeftChild())
					> height((Node<T>) current.getLeftChild().getRightChild())) {
				
				// return rotateLL(current);
				// note that we don't do that because we have to update the
				// height first
				current = rotateLL(current);
			} else {
				current = rotateLR(current);
			}
		}

		else if (height((Node<T>) current.getRightChild())
				- height((Node<T>) current.getLeftChild()) > allowedBallanceLimit) {
			if (height((Node<T>) current.getRightChild().getRightChild())
					> height((Node<T>) current.getRightChild().getLeftChild())) {
				current = rotateRR(current);
			} else {
				current = rotateRL(current);
			}
		}
		current.height = Math.max(height((Node<T>) current.getLeftChild()),
				height((Node<T>) current.getRightChild())) + 1;
		
		return current;
	}

	private Node<T> rotateLL(Node<T> t) {
		System.out.println("---------------<<in LL>>----------------");
		Node<T> temp = (Node<T>) t.getLeftChild() ;
		if(t == root)root = temp ;
		t.setLeftChild((Node<T>) temp.getRightChild());
		temp.setRightChild(t);
		t.height = Math.max(height((Node<T>)t.getLeftChild()),height((Node<T>)t.getRightChild())) + 1;
		temp.height = Math.max(height(t), height((Node<T>)temp.getLeftChild())) + 1;
		return temp;
	}

	private Node<T> rotateLR(Node<T> t) {
		System.out.println("---------------<<in LR>>----------------");
		t.setLeftChild(rotateRR((Node<T>)t.getLeftChild()));
		return rotateLL((Node<T>) t);
	}

	private Node<T> rotateRR(Node<T> t) {
		System.out.println("---------------<<in RR>>----------------");
		Node<T> temp = (Node<T>) t.getRightChild() ;
		if(t == root)root = temp ;
		t.setRightChild((Node<T>) temp.getLeftChild());
		temp.setLeftChild(t);
		t.height = Math.max(height((Node<T>) t.getLeftChild()),height((Node<T>) t.getRightChild())) + 1 ;
		temp.height = Math.max(height((Node<T>) temp.getRightChild()) , height(t)) + 1 ;
		return temp ;
	}

	private Node<T> rotateRL(Node<T> t) {
		System.out.println("---------------<<in RL>>----------------");
		t.setRightChild(rotateLL((Node<T>) t.getRightChild()));
		return rotateRR(t);
	}

	@Override
	public boolean delete(T key) {
		ElementDeleted = false ;
		delete(key, root);
//		System.out.println(ElementDeleted);
		return ElementDeleted;
	}
	
	public Node<T> delete(T key , Node<T> node){
		if(node == null) return null ;
		else{
			int comp = key.compareTo(node.getValue());
			if(comp < 0){
				node.setLeftChild(delete(key , (Node<T>) node.getLeftChild()));
			}else if(comp > 0){
				node.setRightChild(delete(key , (Node<T>) node.getRightChild()));
			}
			else{
				ElementDeleted = true ;
				if(node.getRightChild() != null && node.getLeftChild() != null){
					Node<T> temp = findMin((Node<T>) node.getRightChild()) ; 
					T tempVal = node.getValue() ;
					node.setValue((temp).getValue());
					temp.setValue(tempVal);
					node.setRightChild(delete(key , (Node<T>) node.getRightChild()));				
				}
				else{
					boolean rootDeletion = false;
					if(node == root )rootDeletion = true ;
					if(node.getLeftChild() != null) node = (Node<T>) node.getLeftChild() ;
					else node = (Node<T>) node.getRightChild() ;
					if(rootDeletion)root = node ;
				}
			}
		}
		return balance(node);
	}
	
	private Node<T> findMin(Node<T> t){
		while(t.getLeftChild() != null){
			t = (Node<T>) t.getLeftChild() ;
		}
		return t ;
	}

	@Override
	public boolean search(T key) {
		return (search(key , root) != null);
	}
	
	private INode<T> search(T key , INode<T> node){
		if(node == null) return null ;
		if(key.equals(node.getValue())) return node ;
		else{
			int comp = key.compareTo(node.getValue());
			if(comp < 0){
				return search(key , node.getLeftChild());
			}else{
				return search(key , node.getRightChild());
			}
		}
	}

	@Override
	public int height() {
		if(root == null)return 0 ;
		return root.height;
	}

	@Override
	public INode<T> getTree() {
		return root;
	}
	
	public void inOrderTraversal(Node<T> node){
		if(node == null)return ;
		inOrderTraversal((Node<T>) node.getLeftChild());
		System.out.println(node.getValue() + " height : " + height(node));
		inOrderTraversal((Node<T>) node.getRightChild());
	}
	
	public static void main(String[] args){
//		Avl test = new Avl<>();
//		test.insert("F");
//		test.inOrderTraversal((Node) test.getTree());
//		System.out.println();
//		test.insert("G");
//		test.inOrderTraversal((Node) test.getTree());
//		System.out.println();
//		test.insert("F");
//		test.inOrderTraversal((Node) test.getTree());
//		System.out.println();
//		test.insert("E");
//		test.inOrderTraversal((Node) test.getTree());
//		System.out.println();
//		test.insert("D");
//		test.inOrderTraversal((Node) test.getTree());
//		System.out.println();
//		test.insert("C");
//		test.inOrderTraversal((Node) test.getTree());
//		System.out.println();
//		test.insert("B");
//		test.inOrderTraversal((Node) test.getTree());
//		System.out.println();
//		test.insert("A");
//		test.inOrderTraversal((Node) test.getTree());
//		System.out.println();
//		test.insert("H");
//		test.inOrderTraversal((Node) test.getTree());
//		System.out.println();
//		test.delete("C");
//		test.inOrderTraversal((Node) test.getTree());
//		System.out.println();
//		test.delete("C");
//		test.inOrderTraversal((Node) test.getTree());
//		System.out.println();
//		test.delete("A");
//		test.inOrderTraversal((Node) test.getTree());
//		System.out.println();
//		test.delete("B");
//		test.inOrderTraversal((Node) test.getTree());
//		System.out.println();
//		test.delete("E");
//		test.inOrderTraversal((Node) test.getTree());
//		System.out.println("Root now is " + test.getTree().getValue());
//		System.out.println();
//		test.delete("G");
//		test.inOrderTraversal((Node) test.getTree());
//		System.out.println();
//		test.delete("H");
//		test.inOrderTraversal((Node) test.getTree());
//		System.out.println();
//		System.out.println("Root now is " + test.getTree().getValue());
//		test.delete("H");
//		test.inOrderTraversal((Node) test.getTree());
//		System.out.println();
//		test.delete("D");
//		test.inOrderTraversal((Node) test.getTree());
//		System.out.println();
//		test.delete("F");
//		test.inOrderTraversal((Node) test.getTree());
//		System.out.println();
//		test.delete("F");
//		test.inOrderTraversal((Node) test.getTree());
//		System.out.println();
	}

}
