package eg.edu.alexu.csd.filestructure.avl;

public class Node<T extends Comparable<T>> implements INode<T> {

	private Node<T> left ;
	private Node<T> right ;
	private T value ;
	public int height ;
	
	public Node(T value ,Node<T> left , Node<T> right){
		this.value = value ; 
		this.right = right ; 
		this.left = left ;
		height = 0 ;
	}
	
	@Override
	public INode<T> getLeftChild() {
		return left ;
	}

	@Override
	public INode<T> getRightChild() {
		return right;
	}

	@Override
	public T getValue() {
		return value;
	}

	@Override
	public void setValue(T value) {
		this.value = value ;
	}
	
	public void setRightChild(Node<T> right){
		this.right = right ;
	}

	public void setLeftChild(Node<T> left){
		this.left = left ;
	}
	
//	public int height(){
//		return height ;
//	}
}
