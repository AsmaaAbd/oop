package eg.edu.alexu.csd.filestructure.avl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Dictionary implements IDictionary{

	private IAVLTree<String> avl ;
	
	public Dictionary(){
		avl = new Avl<>() ;
	}
	
	@Override
	public void load(File file) {
		try {
			
			BufferedReader br = new BufferedReader(new FileReader(file));
			String temp = br.readLine() ;
			while(temp != null){
				insert(temp);
				temp = br.readLine() ;
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public boolean insert(String word) {
		boolean exists = exists(word);
		if(!exists)avl.insert(word);
		return !exists;
	}

	@Override
	public boolean exists(String word) {
		return avl.search(word);
	}

	@Override
	public boolean delete(String word) {
		return avl.delete(word);
	}

	@Override
	public int size() {
		return inOrderTraversal(avl.getTree());
	}
	
	private int inOrderTraversal(INode<String> node){
		if(node == null)return 0 ;
		return inOrderTraversal(node.getLeftChild()) + 1 + inOrderTraversal(node.getRightChild()) ;
	}

	@Override
	public int height() {
		return avl.height();
	}

	public static void main(String[] args){
		Dictionary dic = new Dictionary();
		dic.load(new File("dic.txt"));
		System.out.println(dic.size());
	}
	
}
