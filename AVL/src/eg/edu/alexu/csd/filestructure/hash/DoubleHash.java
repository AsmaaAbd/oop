package eg.edu.alexu.csd.filestructure.hash;

import java.util.ArrayList;

public class DoubleHash implements IHash, IHashDouble {

	private NodeWithDeletion[] table ;
	int collisions ;
	int capacity ;
	int nonEmptySlots ;
	int currentNearestPrime ;
	
	public DoubleHash(){
		capacity = 1200 ;
		collisions = 0 ;
		nonEmptySlots = 0 ;
		table = new NodeWithDeletion[capacity] ;
		currentNearestPrime = nearestPrime(capacity);
	}
	
	@Override
	public void put(Object key, Object value) {
		
		if(size() == capacity)rehash();
		int entry = key.hashCode() % capacity ; 
		int hash = entry;
		int step = 1 ;
		long hash2 = h2(key) ;
	
		while(table[hash] != null && !table[hash].deleted && step <= capacity){

			hash =(int) ((key.hashCode() + (step++) *hash2 ) % capacity);
			collisions ++ ;
		}
		if(table[hash] ==null || table[hash].deleted){
			table[hash] = new NodeWithDeletion(key, value);
			table[hash].deleted = false ;
			nonEmptySlots++ ;
		}
		else{
			test(key, value);
		}
	}
	
	private void test(Object key , Object value){
		currentNearestPrime = nearestPrime(2*capacity);
		rehash();
		put(key , value );
	}
	
	private void rehash(){
		collisions += 1  ;
		capacity *= 2 ;
		nonEmptySlots = 0 ;
		
		
		//move elements from the old to the new
		NodeWithDeletion [] temp = table ;
		table = new NodeWithDeletion [capacity];
		for(int i = 0 ; i < temp.length ;i++){
			if(temp[i] != null)put(temp[i].key , temp[i].value) ;
		}
	}

	private int nearestPrime(int cap){
		for(int i = cap ; i >= 2 ; i--){
			int j = 2 ;
			for( ; Math.pow(j, 2)<i ; j++){
				if(i % j == 0)break ;
			}
			if(Math.pow(j, 2)>i){
				System.out.println(i);
				return i ;
			}
		}
		return 2 ;
	}
	
	private long h2(Object key){
		return 1193 - (key.hashCode() % 1193) ;
	}
	
	@Override
	public String get(Object key) {
		int entry = key.hashCode() % capacity ;
		int i = 1 ;
		int hash = entry ;
		while((table[hash] != null && table[hash].key != (int)key)&& i < capacity){
			hash =(int) ((entry + (i++) * h2((int)key)) % capacity);
		}
		if(table[hash] != null && table[hash].key == (int)key && ! table[hash].deleted)return table[hash].value;
		return null ;
	}

	@Override
	public void delete(Object key) {
		int entry = key.hashCode() % capacity ;
		int i = 1 ;
		int hash = entry ;
		while((table[hash] != null && table[hash].key != (int)key)&& i < capacity){
			hash =(int) ((entry + (i++) * h2((int)key)) % capacity);
		}
		if(table[hash] != null && table[hash].key == (int)key){
			nonEmptySlots -- ;
			table[hash].deleted = true ;
		}
	}

	@Override
	public boolean contains(Object key) {
		int entry = key.hashCode() % capacity ;
		int i = 1 ;
		int hash = entry ;
		while((table[hash] != null && table[hash].key != (int)key)&& i < capacity){
			hash = (int)((entry + (i++) * h2((int)key)) % capacity) ;
		}
		if(table[hash] != null && table[hash].key == (int)key && !table[hash].deleted)return true;
		return false;
	}

	@Override
	public boolean isEmpty() {
		return nonEmptySlots == 0;
	}

	@Override
	public int size() {
		return nonEmptySlots;
	}

	@Override
	public int capacity() {
		return capacity;
	}

	@Override
	public int collisions() {
//		throw new RuntimeException(test);
		return collisions;
	}

	@Override
	public Iterable keys() {
		ArrayList<Integer> keys = new ArrayList() ;
		for(int i = 0 ; i < capacity ; i++){
			if(table[i]!=null && !table[i].deleted)keys.add(table[i].key);
		}
		return keys ;
	}
	
	public static void main (String args[]){
		IHash h = new DoubleHash();
//		Scanner sc  = new Scanner (System.in);

		//		h.put(0, "-0-");
		//		h.put(0, "-0-");
		//		h.put(0, "-0-");
		for(int i=0; i<1000; i++){
			int key = (i+100000) * 12345;
			h.put(key, String.valueOf(i));
		}
		System.out.println("finsh");
		System.out.println("size = "+h.size());
		System.out.println("capacity = "+h.capacity());
		System.out.println("collisions =  = "+h.collisions());
		System.out.println("============================================================");
		System.out.println("After rehashing : ");
		h = new DoubleHash() ;
		for(int i=0; i<10000; i++){
			int key = (i+100000) * 12345;
			h.put(key, String.valueOf(i));
		}
		System.out.println("finsh");
		System.out.println("size = "+h.size());
		System.out.println("capacity = "+h.capacity());
		System.out.println("collisions =  = "+h.collisions());
	}

}
