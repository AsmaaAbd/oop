package eg.edu.alexu.csd.filestructure.hash;

import java.util.ArrayList;

public class LinearProbing implements IHash , IHashLinearProbing{

	private Node[] table ;
	private int capacity ;
	private int nonEmptySlots ;
	private int collisions ;
	private String test = new String();
	
	public LinearProbing(){
		capacity = 1200 ;
		table = new Node [capacity];
	}
	
	@Override
	public void put(Object key, Object value) {
		if(nonEmptySlots == capacity){
			collisions+= capacity + 1;
			capacity *= 2 ;
			nonEmptySlots = 0 ;
			
			//move elements from the old to the new
			Node [] temp = table ;
			table = new Node [capacity];
			for(int i = 0 ; i < temp.length ;i++){
				if(temp[i] != null)put(temp[i].key , temp[i].value) ;
			}
		}
		
		test += key + " : " + value + "\n" ;
		int entry = key.hashCode() % capacity ;
		
		boolean first = true ;
		while(table[entry] != null){
			if(first) {
				first = false ;
				collisions ++ ;
			}
			collisions ++ ;
			entry = (entry+1) % capacity;
		}
		table[entry] = new Node(key , value) ;
		nonEmptySlots ++ ;
	}

	@Override
	public String get(Object key) {
		int entry = key.hashCode() % capacity ;
		int i = 0 ;
		while((table[entry] == null || table[entry].key != (int)key) && i < capacity){
			entry = (entry+1) % capacity ;
			i++;
		}
		if(i < capacity)return table[entry].value ;
		return null;
	}

	@Override
	public void delete(Object key) {
		int entry = key.hashCode() % capacity ;
		int i = 0 ;
		while((table[entry] == null || table[entry].key != (int)key) && i < capacity){
			entry = (entry+1) % capacity ;
			i++;
		}
		if(i < capacity){
			nonEmptySlots -- ;
			table[entry] = null ;
		}
	}

	@Override
	public boolean contains(Object key) {
		int entry = key.hashCode() % capacity ;
		int i = 0 ;
		while((table[entry] == null || table[entry].key != (int)key)&& i < capacity){
			entry = (entry+1) % capacity ;
			i++;
		}
		if(i < capacity)return true;
		return false;
	}

	@Override
	public boolean isEmpty() {
		return nonEmptySlots == 0;
	}

	@Override
	public int size() {
		return nonEmptySlots;
	}

	@Override
	public int capacity() {
		return capacity;
	}

	@Override
	public int collisions() {
//		throw new RuntimeException(test);
		return collisions;
	}

	@Override
	public Iterable keys() {
		ArrayList<Integer> keys = new ArrayList() ;
		for(int i = 0 ; i < capacity ; i++){
			if(table[i] != null)keys.add(table[i].key);
		}
		return keys ;
	}

	public static void main(String [] args){
		LinearProbing testClass = new LinearProbing() ;
		testClass.put(3, "Asmaa");
		testClass.put(1203, "Asmaa");
		testClass.put(2403, "Asmaa");
		System.out.println(testClass.table[4]);
//		testClass.delete(1203);
		testClass.delete(2403);
		testClass.delete(6);
		System.out.println(testClass.table[4]);
		System.out.println(testClass.table[5]);
		System.out.println(testClass.contains(1203));
		System.out.println(testClass.collisions());
		System.out.println(testClass.size());
	}
	
}
