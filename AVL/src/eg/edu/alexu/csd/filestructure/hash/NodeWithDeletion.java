package eg.edu.alexu.csd.filestructure.hash;

public class NodeWithDeletion {

	public int key ;
	public String value ;
	public boolean deleted ;
	
	public NodeWithDeletion(Object key2 , Object value2){
		this.key = ((int)key2) ;
		this.value = ((String)value2) ;
		deleted = false ;
	}
	
}
