package eg.edu.alexu.csd.filestructure.hash;

import java.util.ArrayList;

import javax.management.RuntimeErrorException;

public class Chaining<K , V> implements IHash , IHashChaining{

	private ArrayList<Node>[] table ;
	private final int size = 1200 ;
	private int nonEmptySlots ;
	private int collisions ;
	private int keys ;
	private String test = null;
	
	public Chaining(){
		table = new ArrayList [size];
		nonEmptySlots = 0 ;
		collisions = 0 ;
		keys = 0 ;
	}
	
	@Override
	public void put(Object key, Object value) {
		test += key + " : " + value + "\n" ;
		keys++ ;
		int entry = key.hashCode()%size ;
		if(table[entry] == null || table[entry].size() == 0){
			table[entry] = new ArrayList<Node>() ;
			nonEmptySlots++;
		}
		collisions += table[entry].size() ;
		table[entry].add(new Node(key , value)) ;
	}

	@Override
	public String get(Object key) {
		int entry = key.hashCode()%size ;
		if(table[entry] != null){
			for(Node node : table[entry]){
				if(node.key == (int) key){
					return node.value ;
				}
			}
			return null ;
		}
		return null ;
	}

	
	@Override
	public void delete(Object key) {
		int entry = key.hashCode()%size ;
		if(table[entry] != null){
			for(Node node : table[entry]){
				if(node.key == (int) key){
					keys -- ;
					table[entry].remove(node) ;
					if(table[entry].size()==0) nonEmptySlots -- ;
					break ;
				}
			}
		}
	}

	@Override
	public boolean contains(Object key) {
		int entry = key.hashCode()%size ;
		if(table[entry] != null){
			for(Node node : table[entry]){
				if(node.key == (int) key){
					return true ;
				}
			}
			return false ;
		}
		return false;
	}

	@Override
	public boolean isEmpty() {
		return nonEmptySlots == 0;
	}

	@Override
	public int size() {
		return keys ;
	}

	@Override
	public int capacity() {
		return size;
	}

	@Override
	public int collisions() {
		return collisions;
	}

	@Override
	public Iterable keys() {
		ArrayList keys = new ArrayList<>() ;
		for(int i = 0 ; i < table.length ; i++){
			if(table[i] != null){
				for(Node node : table[i]){
					keys.add(node.key) ;
				}
			}
		}
		return keys;
	}
	
}
