package eg.edu.alexu.csd.filestructure.hash;

import java.util.ArrayList;

public class QuadHashing implements IHash , IHashQuadraticProbing{

	private NodeWithDeletion[] table ;
	private int capacity ;
	private int nonEmptySlots ;
	private int collisions ;
	
	public QuadHashing(){
		capacity = 1200 ;
		table = new NodeWithDeletion[capacity] ;
	}
	
	@Override
	public void put(Object key, Object value) {
		if(size() == capacity)rehash() ;
		int entry = key.hashCode() % capacity ;
		int hash = entry ;
		int step = 1 ;
		while(table[entry] != null && !table[entry].deleted && step <= capacity){
			if(step==1)collisions++;
			collisions ++ ;
			entry = (int) ((hash + Math.pow(step++, 2)) % capacity) ;
		}
		if(table[entry] == null || table[entry].deleted){
			table[entry] = new NodeWithDeletion(key, value);
			table[entry].deleted = false ;
			nonEmptySlots ++ ;
		}
		else{
			rehash();
			put(key, value);
		}
	}

	private void rehash(){
		capacity *= 2 ;
		nonEmptySlots = 0 ;
		
		
		//move elements from the old to the new
		NodeWithDeletion [] temp = table ;
		table = new NodeWithDeletion [capacity];
		for(int i = 0 ; i < temp.length ;i++){
			if(temp[i] != null)put(temp[i].key , temp[i].value) ;
		}
	}
	
	@Override
	public String get(Object key) {
		int entry = key.hashCode() % capacity ;
		int i = 1 ;
		int hash = entry ;
		while((table[hash] != null && table[hash].key != (int)key)&& i < capacity){
			hash = (int) ((entry + Math.pow(i++, 2)) % capacity) ;
		}
		if(table[hash] != null && table[hash].key == (int)key && ! table[hash].deleted)return table[hash].value;
		return null ;
	}

	@Override
	public void delete(Object key) {
		int entry = key.hashCode() % capacity ;
		int i = 1 ;
		int hash = entry ;
		while((table[hash] != null && table[hash].key != (int)key)&& i < capacity){
			hash = (int) ((entry + Math.pow(i++, 2)) % capacity) ;
		}
		if(table[hash] != null && table[hash].key == (int)key){
			nonEmptySlots -- ;
			table[hash].deleted = true ;
		}
	}

	@Override
	public boolean contains(Object key) {
		int entry = key.hashCode() % capacity ;
		int i = 1 ;
		int hash = entry ;
		while((table[hash] != null && table[hash].key != (int)key)&& i < capacity){
			hash = (int) ((entry + Math.pow(i++, 2)) % capacity) ;
		}
		if(table[hash] != null && table[hash].key == (int)key && !table[hash].deleted)return true;
		return false;
	}

	@Override
	public boolean isEmpty() {
		return nonEmptySlots == 0;
	}

	@Override
	public int size() {
		return nonEmptySlots;
	}

	@Override
	public int capacity() {
		return capacity;
	}

	@Override
	public int collisions() {
//		throw new RuntimeException(test);
		return collisions;
	}

	@Override
	public Iterable keys() {
		ArrayList<Integer> keys = new ArrayList() ;
		for(int i = 0 ; i < capacity ; i++){
			if(table[i]!=null && !table[i].deleted)keys.add(table[i].key);
		}
		return keys ;
	}
	
	public static void main (String args[]){
		IHash h = new QuadHashing();
//		Scanner sc  = new Scanner (System.in);

		//		h.put(0, "-0-");
		//		h.put(0, "-0-");
		//		h.put(0, "-0-");
		for(int i=0; i<1000; i++){
			int key = (i+100000) * 12345;
			h.put(key, String.valueOf(i));
		}
		System.out.println("finsh");
		System.out.println("size = "+h.size());
		System.out.println("capacity = "+h.capacity());
		System.out.println("collisions =  = "+h.collisions());
		System.out.println("============================================================");
		System.out.println("After rehashing : ");
		h = new QuadHashing() ;
		for(int i=0; i<10000; i++){
			int key = (i+100000) * 12345;
			h.put(key, String.valueOf(i));
		}
		System.out.println("finsh");
		System.out.println("size = "+h.size());
		System.out.println("capacity = "+h.capacity());
		System.out.println("collisions =  = "+h.collisions());
	}

}
