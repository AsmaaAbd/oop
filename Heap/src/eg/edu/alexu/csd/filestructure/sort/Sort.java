package eg.edu.alexu.csd.filestructure.sort;

import java.util.ArrayList;

public class Sort<T extends Comparable<T>> implements ISort<T> {

	private ArrayList<T> A;
	private ArrayList<T> B ;
	
	@Override
	public IHeap<T> heapSort(ArrayList<T> unordered) {
		Heap<T> heap = new Heap<T>();
		return heap.heapSort(unordered);
	}

	@Override
	/*Bubble*/
	public void sortSlow(ArrayList<T> unordered) {
		int length = unordered.size();
		for(int i = 0 ; i < length ;i++){
			T min = unordered.get(i);
			int minInd = i ;
			for(int j = i+1 ; j < length ;j++){
				T potMin = unordered.get(j); 
				if(potMin.compareTo(min) < 0){
					min = potMin;
					minInd = j ;
				}
			}
			unordered.set(minInd, unordered.get(i));
			unordered.set(i, min);
		}
	}

	@Override
	public void sortFast(ArrayList<T> unordered) {
		A = unordered ;
		B = new ArrayList<T>();
		for(int i = 0 ; i < A.size() ; i++){
			B.add(null);
		}
		TopDownSplitMerge(A, 0, A.size(), B);
	}

	private void TopDownSplitMerge(ArrayList<T> A,int iBegin,int iEnd,ArrayList<T> B)
	{
	    if(iEnd - iBegin < 2)                       // if run size == 1
	        return;                                 //   consider it sorted

	    int iMiddle = (iEnd + iBegin) / 2;              // iMiddle = mid point
	    TopDownSplitMerge(A, iBegin,  iMiddle, B);  // split / merge left  half
	    TopDownSplitMerge(A, iMiddle,    iEnd, B);  // split / merge right half
	    TopDownMerge(A, iBegin, iMiddle, iEnd, B);  // merge the two half runs
	    CopyArray(B, iBegin, iEnd, A);              // copy the merged runs back to A
	}

	private void TopDownMerge(ArrayList<T> A,int iBegin,int iMiddle,int iEnd,ArrayList<T> B)
	{
	    int i = iBegin, j = iMiddle;
	    
	    for (int k = iBegin; k < iEnd; k++) {
	        if (i < iMiddle && (j >= iEnd || (A.get(i)).compareTo((T)A.get(j)) <= 0)) {
	            B.set(k , A.get(i));
	            i = i + 1;
	        } else {
	        	B.set(k , A.get(j));
	            j = j + 1;    
	        }
	    } 
	}

	private void CopyArray(ArrayList<T> B,int iBegin,int iEnd, ArrayList<T> A)
	{
	    for(int k = iBegin; k < iEnd; k++)
	        A.set(k, B.get(k));
	}
	
//	public static void main(String[] args){
//		Sort sort = new Sort();
//		ArrayList arr = new ArrayList();
//		arr.add(11);
//		arr.add(2);
//		arr.add(30);
//		arr.add(4);
//		arr.add(5);
//		sort.sortFast(arr);
//		for(int i =0 ; i < arr.size() ;i++){
//			System.out.println(arr.get(i));
//		}
//	}
	
}
