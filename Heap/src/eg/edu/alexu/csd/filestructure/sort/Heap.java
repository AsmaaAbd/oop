package eg.edu.alexu.csd.filestructure.sort;

import java.util.ArrayList;
import java.util.Collection;

public class Heap<T extends Comparable<T>> implements IHeap<T> {

	private ArrayList<Node<T>> heap ;
	
	public Heap(){
		heap = new ArrayList<>();
	}
	
	@Override
	public INode<T> getRoot() {
		if(heap.size() == 0)return null ;
		return heap.get(0) ;
	}

	@Override
	public int size() {
		return heap.size();
	}

	@Override
	public void heapify(INode<T> node) {
		
		INode<T> left = (Node<T>) node.getLeftChild();
		INode<T> right = (Node<T>) node.getRightChild();
		INode<T> largest = node;
		
		if(left != null &&(left.getValue()).compareTo(node.getValue()) > 0){
			largest = left ;
		}else{
			largest = node ;
		}
		if(right != null && (largest.getValue()).compareTo(right.getValue()) < 0){
			largest = right ;
		}
		
		if(largest != node){
			int temp = heap.indexOf(node) ;
			heap.set(heap.indexOf(largest), (Node<T>) node);
			heap.set(temp, (Node<T>) largest);
			heapify(node);
		}
	}

	@Override
	public T extract() {
		if(heap == null || heap.size() == 0)return null ;
		if(heap.size() == 1 ){
			T val = (T) heap.get(0).getValue();
			heap.remove(0);
			return val ;
		}
		Node<T> temp = heap.get(0);
		heap.set(0, heap.remove(heap.size()-1));
		heapify(this.getRoot());
		return (T) temp.getValue();
	}
	

	@Override
	public void insert(T element) {
		Node<T> newNode = new Node<T>();
		newNode.setValue(element);
		heap.add(newNode);
		bottomUpHeapify(newNode);
	}
	
	private void bottomUpHeapify(Node<T> node){
		Node<T> parent = (Node<T>) node.getParent();
		int ind = heap.indexOf(node);
		if(parent == null)return ;
		if((node.getValue()).compareTo(parent.getValue()) > 0){
			heap.set(heap.indexOf(parent), node);
			heap.set(ind, parent);
			bottomUpHeapify(node);
		}
	}

	@Override
	public void build(Collection<T> unordered) {
		heap.clear();
		for(T t : unordered){
			this.insert(t);
		}
	}
	
	public Heap<T> heapSort(ArrayList<T> unordered){
		build(unordered);
		Node<T> newRoot ;
		if(heap.size() == 1)return this ;
		for(int i = heap.size()-1 ; i >= 1 ; i--){
			newRoot = heap.get(i);
			heap.set(i, heap.get(0));
			heap.set(0, newRoot);
			heapifyLimited(newRoot , i);
		}
		return this ;
	}

	public void heapifyLimited(INode<T> node , int end) {
		
		INode<T> left = (Node<T>) node.getLeftChild();
		INode<T> right = (Node<T>) node.getRightChild();
		if(heap.indexOf(left) >= end)left = null ;
		if(heap.indexOf(right) >= end)right = null ;
		INode<T> largest = node;
		
		if(left != null &&(left.getValue()).compareTo(node.getValue()) > 0){
			largest = left ;
		}else{
			largest = node ;
		}
		if(right != null && (largest.getValue()).compareTo(right.getValue()) < 0){
			largest = right ;
		}
		
		if(largest != node){
			int temp = heap.indexOf(node) ;
			heap.set(heap.indexOf(largest), (Node<T>) node);
			heap.set(temp, (Node<T>) largest);
			heapifyLimited(node , end);
		}
	}

	public void print(){
		for(int i = 0 ; i < heap.size() ; i++){
			System.out.println(heap.get(i).getValue());
		}
	}
	
	@SuppressWarnings("hiding")
	private class Node<T extends Comparable<T>> implements INode<T> {

		private T value ; 
		private int indx ;
		
		public Node(){
			value = null;
		}
		
		@SuppressWarnings("unchecked")
		@Override
		public INode<T> getLeftChild() {
			indx = heap.indexOf(this);
			if(indx*2+1 < heap.size())return (INode<T>) heap.get(indx*2 + 1);
			return null;
		}

		@SuppressWarnings("unchecked")
		@Override
		public INode<T> getRightChild() {
			indx = heap.indexOf(this);
			if(indx*2 + 2 < heap.size())return (INode<T>) heap.get(indx*2 + 2);
			return null;
		}

		@SuppressWarnings("unchecked")
		@Override
		public INode<T> getParent() {
			indx = heap.indexOf(this);
			if(indx == 0)return null ;
			if(indx % 2 == 0)return (INode<T>) heap.get(indx / 2 - 1) ;
			return (INode<T>) heap.get(indx / 2);
		}

		@Override
		public void setValue(T value) {
			this.value = value ;
		}

		@Override
		public T getValue() {
			return value;
		}

	}

}
