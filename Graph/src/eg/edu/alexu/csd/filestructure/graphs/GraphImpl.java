package eg.edu.alexu.csd.filestructure.graphs;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.PriorityQueue;

public class GraphImpl implements IGraph {
	
	private ArrayList<Node> graph ;
	private ArrayList<Integer> dijkstraProcessedOrder ;
	HashMap<Integer, Node> dijGraph ;
	private PriorityQueue<Node> prQueue ;
	private int[] parent ;
	private int[] dis ;
	private int e , v ;
	
	//for optimization purposes :
	private int[] added ;
	private int addingOrder ;
	
	public GraphImpl(){
		graph = new ArrayList<Node>() ;
		e = 0 ;
	}

	@Override
	public void readGraph(File file) {
		try {
			@SuppressWarnings("resource")
			BufferedReader br = new BufferedReader(new FileReader(file));
			String edgesAndVertices = br.readLine() ;
			String[] temp = edgesAndVertices.split("\\s+") ;
			v = Integer.parseInt(temp[0]) ;
			e = Integer.parseInt(temp[1]) ;
			
			added = new int[v];
			addingOrder = 1 ;
			dijGraph = new HashMap<>() ;
			
			for(int i = 0 ; i < e ; i++){
				String tempo = br.readLine() ;
				
				temp = tempo.split("\\s+") ;
				int u = Integer.parseInt(temp[0]);
				int to = Integer.parseInt(temp[1]);
				int w = Integer.parseInt(temp[2]);
				
				if(u > v || to > v)throw new RuntimeException() ;
				else if(added[u] != 0)
				{
					graph.get(added[u]-1).adj.add(new Edge(to , w));
				}
				else
				{
					Node current = new Node(u) ;
					graph.add(current);
					current.adj.add(new Edge(to, w)) ;
					dijGraph.put(u, current) ; 
					added[u] = addingOrder ++ ;
				}
			}
		} catch (Exception e) {
			throw new RuntimeException() ;
		}
	}

	@Override
	public int size() {
		return e ;
	}

	@Override
	public ArrayList<Integer> getVertices() {
		ArrayList <Integer> vertices = new ArrayList<Integer>() ;
		for(int i = 0 ; i < v ;i++ ){
			vertices.add(i) ;
		}
		return vertices;
	}

	@Override
	public ArrayList<Integer> getNeighbors(int v) {
		ArrayList<Integer> ret = new ArrayList<Integer>() ;
		
		for(Edge edge : graph.get(v).adj){
			ret.add(edge.to) ;
		}
		return ret;
	}

	@Override
	public void runDijkstra(int src, int[] distances) {
		
		dis = distances ;
		prQueue = new PriorityQueue<Node>();
		dijkstraProcessedOrder = new ArrayList<Integer>() ;
		
		Node srcNode = null;
		for(Node node : graph){
			if(node.vertex == src){
				srcNode = node ;
				node.dis = 0 ;
			}
			else{
				node.dis = (int) (Float.POSITIVE_INFINITY / 2) ;				
			}
			dis[node.vertex] = node.dis ;
		}
		
		prQueue.add(srcNode);
		while(!graph.isEmpty()){
			Node temp = prQueue.poll() ;	
			graph.remove(temp) ;
			dijkstraProcessedOrder.add(temp.vertex);
			relax2(temp) ;
		}
	}
	
	private boolean relax2(Node u){
		boolean relaxed = false ;
		for(Edge edge : u.adj){
			Node vv = dijGraph.get(edge.to) ;
			if(vv.dis > u.dis + edge.w) {
				relaxed = true ;
				vv.dis = u.dis + edge.w ;
				dis[vv.vertex] = vv.dis ;
				//add or update :3 ;
				if(!prQueue.contains(vv))prQueue.add(vv);
			}
		}
		return relaxed ;
	}

	@Override
	public ArrayList<Integer> getDijkstraProcessedOrder() {
		return dijkstraProcessedOrder;
	}
 
	@Override
	public boolean runBellmanFord(int src, int[] distances) {
		int i = 1 ; 
		
		for(int k = 0 ; k < distances.length ; k++){
			distances[k] = (int) Float.POSITIVE_INFINITY / 2 ;
		}
		distances[src] = 0 ;
		dis = distances ;
		parent = new int[dis.length] ;
		
		for( ;i < v ; i++){
			for(Node node : graph){
				relax(node) ;
			}
		}
		
		for(Node node : graph){
			if(relax(node))return false;
		}
		return true;
	}
	
	//relaxes all edges of this vertex
	private boolean relax(Node u){
		boolean relaxed = false ;
		for(Edge edge : u.adj){
			int v = edge.to ;
			if(dis[v] > dis[u.vertex] + edge.w) {
				relaxed = true ;
				dis[v] = dis[u.vertex] + edge.w ;
				parent[v] = u.vertex ;
			}
		}
		return relaxed ;
	}

//	public static void main(String[] args){
//		GraphImpl test = new GraphImpl() ;
//		test.readGraph(new File("input.txt"));
//		System.out.println(test.getVertices());
//		ArrayList<Integer> nei = test.getNeighbors(1);
//		for(int i = 0 ; i < nei.size() ; i++){
//			System.out.println(nei.get(i));
//		}
//		int [] dis = new int[test.getVertices().size()] ;
////		test.runBellmanFord(0,dis ) ;
//		test.runDijkstra(0, dis);
//		ArrayList<Integer> show = test.getDijkstraProcessedOrder() ;
//		for(int k = 0 ; k < show.size() ; k++){
//			System.out.println(show.get(k));
//		}
//		
//		
//	}
	
}
