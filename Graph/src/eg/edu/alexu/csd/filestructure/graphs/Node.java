package eg.edu.alexu.csd.filestructure.graphs;

import java.util.ArrayList;

public class Node implements Comparable<Node>{
	
	int vertex ;
	ArrayList<Edge> adj ;
	int dis ; 
	
	public Node(int v){
		this.vertex = v ;
		adj = new ArrayList<Edge>();
	}

	@Override
	public int compareTo(Node o) {
		return this.dis - o.dis;
	}

}
